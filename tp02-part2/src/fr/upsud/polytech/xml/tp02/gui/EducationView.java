package fr.upsud.polytech.xml.tp02.gui;

import fr.upsud.polytech.xml.tp02.model.Education;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

public class EducationView extends VBox
{
    private Education education;
    private TextField txtSchool;
    private TextField txtYear;

    public EducationView(Education education)
    {
        this.education = education;

        build();
    }

    private void build()
    {
        HBox hBoxContainer = new HBox();

        txtSchool = new TextField(education.getResume());
        txtSchool.textProperty().addListener(event -> education.setResume(txtSchool.getText()));
        txtSchool.setPromptText("School");

        txtYear = new TextField(Integer.toString(education.getAnnee()));
        txtYear.textProperty().addListener(event -> education.setAnnee(Integer.parseInt(txtYear.getText())));
        txtYear.setPromptText("Year");

        hBoxContainer.getChildren().addAll(txtSchool, txtYear);
        getChildren().addAll(hBoxContainer );
    }

    public Education getEducation()
    {
        return education;
    }
}
