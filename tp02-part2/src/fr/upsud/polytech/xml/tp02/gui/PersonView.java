package fr.upsud.polytech.xml.tp02.gui;

import fr.upsud.polytech.xml.tp02.model.Curriculum;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

public class PersonView extends VBox
{
    private Curriculum person;
    private TextField txfName;
    private TextField txfLastName;
    private TextField txtAge;
    private TextField txfAdresse;
    private TextField txfCity;
    private TextField txfCityCode;

    public PersonView(Curriculum person)
    {
        this.person = person;

        build();
    }

    private void build()
    {
        HBox hBoxIdentity = new HBox();
        txfName = new TextField(person.getPrenom());
        txfName.textProperty().addListener(event -> person.setPrenom(txfName.getText()));
        txfName.setPromptText("Name");

        txfLastName = new TextField(person.getNom());
        txfLastName.textProperty().addListener(event -> person.setNom(txfLastName.getText()));
        txfLastName.setPromptText("Last name");

        txtAge = new TextField(Integer.toString(person.getAge()));
        txtAge.textProperty().addListener(event -> person.setAge(Integer.parseInt(txtAge.getText())));
        txtAge.setPromptText("Age");

        hBoxIdentity.getChildren().addAll(txfName, txfLastName, txtAge);

        HBox hBoxAddress = new HBox();

        txfAdresse = new TextField(person.getAdresse());
        txfAdresse.textProperty().addListener(event -> person.setAdresse(txfAdresse.getText()));
        txfAdresse.setPromptText("Address");

        txfCity = new TextField(person.getVille());
        txfCity.textProperty().addListener(event -> person.setVille(txfCity.getText()));
        txfCity.setPromptText("City");

        txfCityCode = new TextField(person.getCodePostal());
        txfCityCode.textProperty().addListener(event -> person.setCodePostal(txfCityCode.getText()));
        txfCityCode.setPromptText("City code");

        hBoxAddress.getChildren().addAll(txfAdresse, txfCity, txfCityCode);

        getChildren().addAll(hBoxIdentity, hBoxAddress);
    }

    public Curriculum getPerson()
    {
        return person;
    }
}
