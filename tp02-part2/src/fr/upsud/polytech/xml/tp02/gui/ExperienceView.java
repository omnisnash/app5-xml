package fr.upsud.polytech.xml.tp02.gui;

import fr.upsud.polytech.xml.tp02.model.Experience;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

public class ExperienceView extends VBox
{
    private Experience experience;
    private TextField txfCompany;
    private TextField dpkDateFrom;
    private TextField dpkDateTo;

    public ExperienceView(Experience experience)
    {
        this.experience = experience;

        build();
    }

    private void build()
    {
        HBox hBoxContainer = new HBox();

        txfCompany = new TextField(experience.getResume());
        txfCompany.textProperty().addListener(event -> experience.setResume(txfCompany.getText()));
        txfCompany.setPromptText("Company");

        dpkDateFrom = new TextField(Integer.toString(experience.getDebut()));
        dpkDateFrom.setOnAction(event -> experience.setDebut(Integer.parseInt(dpkDateFrom.getText())));
        dpkDateFrom.setPromptText("From year");

        dpkDateTo = new TextField(Integer.toString(experience.getFin()));
        dpkDateTo.setOnAction(event -> experience.setFin(Integer.parseInt(dpkDateTo.getText())));
        dpkDateTo.setPromptText("To year");

        hBoxContainer.getChildren().addAll(dpkDateFrom, dpkDateTo);
        getChildren().addAll(txfCompany, hBoxContainer);
    }

    public Experience getExperience()
    {
        return experience;
    }
}
