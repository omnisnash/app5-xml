package fr.upsud.polytech.xml.tp02.model;

public class Skill
{
    private String competence;
    private String niveau;

    public Skill()
    {
    }

    public Skill(String competence, String niveau)
    {
        this.competence = competence;
        this.niveau = niveau;
    }

    public String getCompetence()
    {
        return competence;
    }

    public void setCompetence(String competence)
    {
        this.competence = competence;
    }

    public String getNiveau()
    {
        return niveau;
    }

    public void setNiveau(String niveau)
    {
        this.niveau = niveau;
    }
}
