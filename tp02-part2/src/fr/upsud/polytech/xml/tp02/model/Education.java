package fr.upsud.polytech.xml.tp02.model;

import java.time.LocalDate;

public class Education
{
    private String resume;
    private int annee;

    public Education()
    {
    }

    public Education(String resume, int annee)
    {

        this.resume = resume;
        this.annee = annee;
    }

    public String getResume()
    {
        return resume;
    }

    public void setResume(String resume)
    {
        this.resume = resume;
    }

    public int getAnnee()
    {
        return annee;
    }

    public void setAnnee(int annee)
    {
        this.annee = annee;
    }
}