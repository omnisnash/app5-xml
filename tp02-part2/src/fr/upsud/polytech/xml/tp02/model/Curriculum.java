package fr.upsud.polytech.xml.tp02.model;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

public class Curriculum
{
    private String prenom;
    private String nom;
    private int age;
    private String adresse;
    private String codePostal;
    private String ville;
    private List<Education> formations;
    private List<Experience> experiences;
    private List<Skill> competences;

    public Curriculum()
    {
        formations = new ArrayList<>();
        competences = new ArrayList<>();
        experiences = new ArrayList<>();
    }


    public String toJson() {
        Gson gson = new Gson();
        return gson.toJson(this);
    }

    public static Curriculum fromJson(String json) {
        Gson gson = new Gson();
        return gson.fromJson(json, Curriculum.class);
    }

    public String getPrenom()
    {
        return prenom;
    }

    public void setPrenom(String prenom)
    {
        this.prenom = prenom;
    }

    public String getNom()
    {
        return nom;
    }

    public void setNom(String nom)
    {
        this.nom = nom;
    }

    public int getAge()
    {
        return age;
    }

    public void setAge(int age)
    {
        this.age = age;
    }

    public String getAdresse()
    {
        return adresse;
    }

    public void setAdresse(String adresse)
    {
        this.adresse = adresse;
    }

    public String getCodePostal()
    {
        return codePostal;
    }

    public void setCodePostal(String codePostal)
    {
        this.codePostal = codePostal;
    }

    public String getVille()
    {
        return ville;
    }

    public void setVille(String ville)
    {
        this.ville = ville;
    }

    public List<Education> getFormations()
    {
        return formations;
    }

    public void setFormations(List<Education> formations)
    {
        this.formations = formations;
    }

    public List<Experience> getExperiences()
    {
        return experiences;
    }

    public void setExperiences(List<Experience> experiences)
    {
        this.experiences = experiences;
    }

    public List<Skill> getCompetences()
    {
        return competences;
    }

    public void setCompetences(List<Skill> competences)
    {
        this.competences = competences;
    }
}
