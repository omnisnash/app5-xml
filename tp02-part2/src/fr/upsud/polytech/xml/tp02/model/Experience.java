package fr.upsud.polytech.xml.tp02.model;

import java.time.LocalDate;

public class Experience
{
    private String resume;
    private int debut;
    private int fin;

    public Experience()
    {
    }

    public Experience(String resume, int debut, int fin)
    {
        this.resume = resume;
        this.debut = debut;
        this.fin = fin;
    }

    public String getResume()
    {
        return resume;
    }

    public void setResume(String resume)
    {
        this.resume = resume;
    }

    public int getDebut()
    {
        return debut;
    }

    public void setDebut(int debut)
    {
        this.debut = debut;
    }

    public int getFin()
    {
        return fin;
    }

    public void setFin(int fin)
    {
        this.fin = fin;
    }
}
