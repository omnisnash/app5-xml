package fr.upsud.polytech.xml.tp02;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonWriter;
import fr.upsud.polytech.xml.tp02.gui.CurriculumView;
import fr.upsud.polytech.xml.tp02.model.Curriculum;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.ScrollPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.*;

public class Tp02Application extends Application implements CurriculumView.CurriculumViewEvents
{

    private ScrollPane scrollPane;
    private CurriculumView curriculumView;
    private Stage stage;

    public static void main(String[] args)
    {
        launch(args);
    }

    @Override public void start(Stage primaryStage)
    {
        stage = primaryStage;
        curriculumView = new CurriculumView(new Curriculum(), this);
        scrollPane = new ScrollPane(curriculumView);

        Scene scene = new Scene(scrollPane, 800, 600);

        primaryStage.setScene(scene);

        primaryStage.show();
    }

    @Override public void onLoad()
    {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open Resource File");
        fileChooser.setInitialFileName("cv.json");
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("Json CV", "*.json")
        );

        File selectedFile = fileChooser.showOpenDialog(stage);
        if (selectedFile != null) {

            try (Reader reader = new FileReader(selectedFile)) {
                Gson gson = new GsonBuilder().create();
                Curriculum curriculum = gson.fromJson(reader, Curriculum.class);
                scrollPane.setContent(new CurriculumView(curriculum, this));
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }
    }

    @Override public void onSave()
    {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open Resource File");
        fileChooser.setInitialFileName("cv.json");
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("Json CV", "*.json")
        );

        File selectedFile = fileChooser.showSaveDialog(stage);
        if (selectedFile != null) {

            try (Writer writer = new FileWriter(selectedFile)) {
                Gson gson = new GsonBuilder().create();
                gson.toJson(curriculumView.getCurriculum(), writer);
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }
    }
}
