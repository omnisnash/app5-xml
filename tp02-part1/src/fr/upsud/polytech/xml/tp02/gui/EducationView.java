package fr.upsud.polytech.xml.tp02.gui;

import fr.upsud.polytech.xml.tp02.model.Education;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

public class EducationView extends VBox
{
    private Education education;
    private TextField txtSchool;
    private DatePicker dpkDateFrom;
    private DatePicker dpkDateTo;
    private TextArea txaDescription;

    public EducationView(Education education)
    {
        this.education = education;

        build();
    }

    private void build()
    {
        HBox hBoxContainer = new HBox();

        txtSchool = new TextField(education.getSchool());
        txtSchool.textProperty().addListener(event -> education.setSchool(txtSchool.getText()));
        txtSchool.setPromptText("School");

        dpkDateFrom = new DatePicker(education.getFromDate());
        dpkDateFrom.setOnAction(event -> education.setFromDate(dpkDateFrom.getValue()));
        dpkDateFrom.setPromptText("From date");

        dpkDateTo = new DatePicker(education.getToDate());
        dpkDateTo.setOnAction(event -> education.setToDate(dpkDateTo.getValue()));
        dpkDateTo.setPromptText("To date");

        txaDescription = new TextArea(education.getDescription());
        txaDescription.textProperty().addListener(event -> education.setDescription(txaDescription.getText()));
        txaDescription.setPromptText("Description");
        txaDescription.setPrefRowCount(3);

        hBoxContainer.getChildren().addAll(txtSchool, dpkDateFrom, dpkDateTo);
        getChildren().addAll(hBoxContainer, txaDescription);
    }

    public Education getEducation()
    {
        return education;
    }
}
