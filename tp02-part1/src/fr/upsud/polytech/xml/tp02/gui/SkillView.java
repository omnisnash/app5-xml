package fr.upsud.polytech.xml.tp02.gui;

import fr.upsud.polytech.xml.tp02.model.Skill;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;

public class SkillView extends HBox {

    private Skill skill;
    private TextField txfName;
    private TextField txfLevel;

    public SkillView(Skill skill)
    {
        this.skill = skill;

        build();
    }

    private void build()
    {
        txfName = new TextField(skill.getName());
        txfName.textProperty().addListener(event -> skill.setName(txfName.getText()));
        txfName.setPromptText("Name");

        txfLevel = new TextField(skill.getLevel());
        txfLevel.textProperty().addListener(event -> skill.setLevel(txfLevel.getText()));
        txfLevel.setPromptText("Level");

        getChildren().addAll(txfName, txfLevel);
    }

    public Skill getSkill()
    {
        return skill;
    }
}
