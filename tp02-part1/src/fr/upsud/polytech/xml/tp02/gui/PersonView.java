package fr.upsud.polytech.xml.tp02.gui;

import fr.upsud.polytech.xml.tp02.model.Person;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;

public class PersonView extends HBox
{
    private Person person;
    private TextField txfName;
    private TextField txfLastName;

    public PersonView(Person person)
    {
        this.person = person;

        build();
    }

    private void build()
    {
        txfName = new TextField(person.getName());
        txfName.textProperty().addListener(event -> person.setName(txfName.getText()));
        txfName.setPromptText("Name");

        txfLastName = new TextField(person.getLastName());
        txfLastName.textProperty().addListener(event -> person.setLastName(txfLastName.getText()));
        txfLastName.setPromptText("Last name");

        getChildren().addAll(txfName, txfLastName);
    }

    public Person getPerson()
    {
        return person;
    }
}
