package fr.upsud.polytech.xml.tp02.gui;

import fr.upsud.polytech.xml.tp02.model.Experience;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

public class ExperienceView extends VBox
{
    private Experience experience;
    private TextField txfCompany;
    private DatePicker dpkDateFrom;
    private DatePicker dpkDateTo;
    private TextArea txaDescription;

    public ExperienceView(Experience experience)
    {
        this.experience = experience;

        build();
    }

    private void build()
    {
        HBox hBoxContainer = new HBox();

        txfCompany = new TextField(experience.getCompany());
        txfCompany.textProperty().addListener(event -> experience.setCompany(txfCompany.getText()));
        txfCompany.setPromptText("Company");

        dpkDateFrom = new DatePicker(experience.getFromDate());
        dpkDateFrom.setOnAction(event -> experience.setFromDate(dpkDateFrom.getValue()));
        dpkDateFrom.setPromptText("From date");

        dpkDateTo = new DatePicker(experience.getToDate());
        dpkDateTo.setOnAction(event -> experience.setToDate(dpkDateTo.getValue()));
        dpkDateTo.setPromptText("To date");

        txaDescription = new TextArea(experience.getDescription());
        txaDescription.textProperty().addListener(event -> experience.setDescription(txaDescription.getText()));
        txaDescription.setPromptText("Description");
        txaDescription.setPrefRowCount(3);

        hBoxContainer.getChildren().addAll(txfCompany, dpkDateFrom, dpkDateTo);
        getChildren().addAll(hBoxContainer, txaDescription);
    }

    public Experience getExperience()
    {
        return experience;
    }
}
