package fr.upsud.polytech.xml.tp02.gui;

import fr.upsud.polytech.xml.tp02.model.Curriculum;
import fr.upsud.polytech.xml.tp02.model.Education;
import fr.upsud.polytech.xml.tp02.model.Experience;
import fr.upsud.polytech.xml.tp02.model.Skill;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;

import java.time.LocalDate;

public class CurriculumView extends GridPane
{
    private Curriculum curriculum;
    private CurriculumViewEvents callback;
    private VBox vbxExperiences;
    private VBox vbxEducations;
    private VBox vbxSkill;

    public CurriculumView(Curriculum curriculum, CurriculumViewEvents callback)
    {
        this.curriculum = curriculum;
        this.callback = callback;

        build();
    }

    private void build()
    {
        int rowIndex = -1;

        // Person
        add(new Label("Identity"), 0, ++rowIndex);
        add(new PersonView(curriculum.getPerson()), 0, ++rowIndex);

        // Experiences
        add(new Label("Experiences"), 0, ++rowIndex);

        vbxExperiences = new VBox();

        Button btnAddExp = new Button("Add experiences");
        btnAddExp.setOnAction(event -> {
            Experience experience = new Experience();
            curriculum.getExperienceHistory().add(experience);
            createExperienceView(vbxExperiences, curriculum, experience);
        });
        add(btnAddExp, 0, ++rowIndex);

        curriculum.getExperienceHistory().forEach(exp -> createExperienceView(vbxExperiences, curriculum, exp));
        add(vbxExperiences, 0, ++rowIndex);

        // Educations
        add(new Label("Education"), 0, ++rowIndex);

        vbxEducations = new VBox();

        Button btnAddEduc = new Button("Add education");
        btnAddEduc.setOnAction(event -> {
            Education education = new Education();
            curriculum.getEducationHistory().add(education);
            createEductionView(vbxEducations, curriculum, education);
        });
        add(btnAddEduc, 0, ++rowIndex);

        curriculum.getEducationHistory().forEach(educ -> createEductionView(vbxEducations, curriculum, educ));
        add(vbxEducations, 0, ++rowIndex);

        // Skill
        add(new Label("Skill"), 0, ++rowIndex);

        vbxSkill = new VBox();

        Button btnAddSkill = new Button("Add skill");
        btnAddSkill.setOnAction(event -> {
            Skill skill = new Skill("", "");
            curriculum.getSkills().add(skill);
            createSkillView(vbxSkill, curriculum, skill);
        });
        add(btnAddSkill, 0, ++rowIndex);

        curriculum.getSkills().forEach(skill -> createSkillView(vbxSkill, curriculum, skill));
        add(vbxSkill, 0, ++rowIndex);

        // Save
        Button btnSave = new Button("Save");
        btnSave.setOnAction(event -> callback.onSave());
        add(btnSave, 0, ++rowIndex);

        // Load
        Button btnLoad = new Button("Load");
        btnLoad.setOnAction(event -> callback.onLoad());
        add(btnLoad, 0, ++rowIndex);
    }

    private void createExperienceView(Pane container, Curriculum curriculum, Experience experience) {
        GridPane grpContainer = new GridPane();

        grpContainer.add(new ExperienceView(experience), 0, 0);

        Button btnRemove = new Button("-");
        btnRemove.setOnAction(event -> {
            curriculum.getExperienceHistory().remove(experience);
            container.getChildren().remove(grpContainer);
        });
        grpContainer.add(btnRemove, 1, 0);
        container.getChildren().add(grpContainer);
    }

    private void createSkillView(Pane container, Curriculum curriculum, Skill skill) {
        GridPane grpContainer = new GridPane();

        grpContainer.add(new SkillView(skill), 0, 0);

        Button btnRemove = new Button("-");
        btnRemove.setOnAction(event -> {
            curriculum.getSkills().remove(skill);
            container.getChildren().remove(grpContainer);
        });
        grpContainer.add(btnRemove, 1, 0);
        container.getChildren().add(grpContainer);
    }

    private void createEductionView(Pane container, Curriculum curriculum, Education education) {
        GridPane grpContainer = new GridPane();

        grpContainer.add(new EducationView(education), 0, 0);

        Button btnRemove = new Button("-");
        btnRemove.setOnAction(event -> {
            curriculum.getEducationHistory().remove(education);
            container.getChildren().remove(grpContainer);
        });
        grpContainer.add(btnRemove, 1, 0);
        container.getChildren().add(grpContainer);
    }

    public Curriculum getCurriculum()
    {
        return curriculum;
    }

    public interface CurriculumViewEvents {

        void onLoad();

        void onSave();
    }
}
