package fr.upsud.polytech.xml.tp02.model;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

public class Curriculum
{
    private Person person;
    private List<Education> educationHistory;
    private List<Experience> experienceHistory;
    private List<Skill> skills;

    public Curriculum()
    {
        person = new Person();
        educationHistory = new ArrayList<>();
        experienceHistory = new ArrayList<>();
        skills = new ArrayList<>();
    }

    public String toJson() {
        Gson gson = new Gson();
        return gson.toJson(this);
    }

    public static Curriculum fromJson(String json) {
        Gson gson = new Gson();
        return gson.fromJson(json, Curriculum.class);
    }

    public Person getPerson()
    {
        return person;
    }

    public void setPerson(Person person)
    {
        this.person = person;
    }

    public List<Education> getEducationHistory()
    {
        return educationHistory;
    }

    public void setEducationHistory(List<Education> educationHistory)
    {
        this.educationHistory = educationHistory;
    }

    public List<Experience> getExperienceHistory()
    {
        return experienceHistory;
    }

    public void setExperienceHistory(List<Experience> experienceHistory)
    {
        this.experienceHistory = experienceHistory;
    }

    public List<Skill> getSkills()
    {
        return skills;
    }

    public void setSkills(List<Skill> skills)
    {
        this.skills = skills;
    }
}
