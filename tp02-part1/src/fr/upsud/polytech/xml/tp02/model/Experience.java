package fr.upsud.polytech.xml.tp02.model;

import java.time.LocalDate;

public class Experience
{
    private String company;
    private LocalDate fromDate;
    private LocalDate toDate;
    private String description;

    public Experience()
    {
        fromDate = LocalDate.now();
        toDate = LocalDate.now();
    }

    public Experience(String company, LocalDate fromDate, LocalDate toDate, String description)
    {
        this.company = company;
        this.fromDate = fromDate;
        this.toDate = toDate;
        this.description = description;
    }

    public String getCompany()
    {
        return company;
    }

    public void setCompany(String company)
    {
        this.company = company;
    }

    public LocalDate getFromDate()
    {
        return fromDate;
    }

    public void setFromDate(LocalDate fromDate)
    {
        this.fromDate = fromDate;
    }

    public LocalDate getToDate()
    {
        return toDate;
    }

    public void setToDate(LocalDate toDate)
    {
        this.toDate = toDate;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }
}
