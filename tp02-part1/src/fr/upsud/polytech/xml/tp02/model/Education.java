package fr.upsud.polytech.xml.tp02.model;

import java.time.LocalDate;

public class Education
{
    private String school;
    private LocalDate fromDate;
    private LocalDate toDate;
    private String description;

    public Education()
    {
        fromDate = LocalDate.now();
        toDate = LocalDate.now();
    }

    public Education(String school, LocalDate fromDate, LocalDate toDate, String description)
    {
        this.school = school;
        this.fromDate = fromDate;
        this.toDate = toDate;
        this.description = description;
    }

    public String getSchool()
    {
        return school;
    }

    public void setSchool(String school)
    {
        this.school = school;
    }

    public LocalDate getFromDate()
    {
        return fromDate;
    }

    public void setFromDate(LocalDate fromDate)
    {
        this.fromDate = fromDate;
    }

    public LocalDate getToDate()
    {
        return toDate;
    }

    public void setToDate(LocalDate toDate)
    {
        this.toDate = toDate;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }
}
